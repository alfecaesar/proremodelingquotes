<?php 

/* Template Name: System Default */

get_header(); ?>

<main> 
<section class="dark-bg v1-btn bg-image flex-reverse text-left t1 fixed-contact" id="FixedContactStructureForm">
            <?php echo do_shortcode('[contact-form-7 id="297" title="Free Estimate"]'); ?>
        </section>
        <div class="icobalt ilayout" id="MainZone">
            <section class="sub-banner t5 dark-bg bg-image text-left" id="SubBanner" style="background-image:url('<?php $banner_image = get_field( 'banner_image' ); if ( $banner_image ){echo esc_url( $banner_image['url'] ); } ?>')">
                <div class="main">
                    <div class="info title-font">
                        <strong class="header-flair"><?php the_field( 'banner_title' ); ?></strong>
                    </div>
                </div>
            </section>
        </div>         
        <section class="wide-content-area content-style light-bg" id="WideContentArea">
            <article class="main thin" id="MainContent" data-content="true">
                <?php the_content(); ?>
            </article>
        </section>    

        <?php echo do_shortcode('[WP-Coder id="8"]'); ?>
    </main> 

<?php get_footer(); ?>