<?php

show_admin_bar(false);


add_filter('use_block_editor_for_post', '__return_false');



remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

function wpse_wpautop_nobr( $content ) {
    return wpautop( $content, false );
}

add_filter( 'the_content', 'wpse_wpautop_nobr' );
add_filter( 'the_excerpt', 'wpse_wpautop_nobr' );



function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


///////////////////////////////////////////////////////////////
/////////////////////////  WIDGETS ////////////////////////////
///////////////////////////////////////////////////////////////

function create_cwgt() {
    
}
add_action( 'widgets_init', 'create_cwgt' );



///////////////////////////////////////////////////////////////
////////////////////  CUSTOM POST TYPE ////////////////////////
///////////////////////////////////////////////////////////////


function create_cpt() {

    // Windows
    register_post_type( 'windows',
        array(
            'labels' => array(
                'name' => __( 'Windows' ),
                'singular_name' => __( 'Window' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'windows'),
            'show_in_rest' => true,
        )
    );
    
    // Roofs
    register_post_type( 'roofs',
        array(
            'labels' => array(
                'name' => __( 'Roofs' ),
                'singular_name' => __( 'Roof' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'roofs'),
            'show_in_rest' => true,
        )
    );

    

    // Floors
    register_post_type( 'floors',
        array(
            'labels' => array(
                'name' => __( 'Floors' ),
                'singular_name' => __( 'Floor' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'floors'),
            'show_in_rest' => true,
        )
    );

    // Solar
    register_post_type( 'solar',
        array(
            'labels' => array(
                'name' => __( 'Solar' ),
                'singular_name' => __( 'Solar' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'solar'),
            'show_in_rest' => true,
        )
    );

    // HVAC
    register_post_type( 'hvac',
        array(
            'labels' => array(
                'name' => __( 'HVAC' ),
                'singular_name' => __( 'HVAC' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'hvac'),
            'show_in_rest' => true,
        )
    );

    // Kitchen
    register_post_type( 'kitchen',
        array(
            'labels' => array(
                'name' => __( 'Kitchen' ),
                'singular_name' => __( 'Kitchen' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'kitchen'),
            'show_in_rest' => true,
        )
    );

    // Bathroom
    register_post_type( 'bathroom',
        array(
            'labels' => array(
                'name' => __( 'Bathroom' ),
                'singular_name' => __( 'Bathroom' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'bathroom'),
            'show_in_rest' => true,
        )
    );

    flush_rewrite_rules( false );

}
add_action( 'init', 'create_cpt' );
