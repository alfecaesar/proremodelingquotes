<?php
/*
Template Name: Services Template
Template Post Type: roofs, windows, floors, solar, hvac, kitchen, bathroom
*/

get_header();

?>

<main> 

<?php 
                if (have_posts()) :
                    while (have_posts()) :
                        the_post();
        ?>

<section class="dark-bg v1-btn bg-image flex-reverse text-left t1 fixed-contact" id="FixedContactStructureForm">
            <?php echo do_shortcode('[contact-form-7 id="297" title="Free Estimate"]'); ?>
        </section>
        <div class="icobalt ilayout" id="MainZone">


            <section class="banner-area t11 dark-bg bg-image v1-btn text-left" id="BannerAreaT11" style="background-image:url('<?php $banner_image = get_field( 'banner_image' ); if ( $banner_image ){echo esc_url( $banner_image['url'] ); } ?>')" data-onvisible="slide-n-fade">
            <div class="main">
                <div class="info">
                    <span class="title-font"><strong>ABSOLUTELY FREE ESTIMATE FOR</strong> </span>
                    <em><b><?php the_field( 'banner_title' ); ?></b> </em> 
                    <p>Trust us by giving a chance<br> it sould be competitive from all your local pros,<br> Just fill this form, It's Free !!!. </p>
                </div>
            </div>
        </section>

        </div>         
          

        <?php echo do_shortcode('[WP-Coder id="1"]'); ?>

        <?php echo do_shortcode('[WP-Coder id="2"]'); ?>

        <?php echo do_shortcode('[WP-Coder id="4"]'); ?>

        <?php 
                    endwhile;
                endif;
    ?>

    </main> 

<?php get_footer(); ?>