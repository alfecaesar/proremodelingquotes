<?php 

/* Template Name: Home */

get_header(); ?>

<main class="icobalt ilayout" id="MainZone">

        <section class="dark-bg v1-btn bg-image flex-reverse text-left t1 fixed-contact" id="FixedContactStructureForm">
            <?php echo do_shortcode('[contact-form-7 id="297" title="Free Estimate"]'); ?>
        </section>
        
        <section class="banner-area t11 dark-bg bg-image v1-btn text-left" id="BannerAreaT11" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/panels/mainstage-bg-dark.jpg');" data-onvisible="slide-n-fade">
            <div class="main">
                <div class="info">
                    <span class="title-font"><strong>TRUST YOUR Home NEEDS TO</strong> </span>
                    <em><b>PRO REMODELING QUOTES</b> </em>
                    <p>We are specialists in estimates for residential projects of <br> roofing & flooring, windows & doors, heat, vent & cooling,<br> solar panels and many more to make your dream home.</p>
                    <ul class="cta-list items-2">
                        <li data-item="i">
                            <a href="/windows-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/stormwindows.jpg')" alt="Windows"> <span>Windows</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/roofing-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/AsphaltRoof.jpg')" alt="Residential Roofing"> <span>Roofing</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/flooring-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/Pine.jpg')" alt="Residential Roofing"> <span>Flooring</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/solar-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/panels.jpg')" alt="Commercial Roofing"> <span>Solar Panels</span> </a>
                        </li>
                    </ul>
                    <ul class="cta-list items-2">
                        <li data-item="i">
                            <a><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/patio-doors.jpg')" alt="Commercial Roofing"> <span>Doors</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/hvac-systems/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/hvac.jpg')" alt="Residential Roofing"> <span>Heat, Vent & Cooling</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/kitchen-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/kitchen remodeling.jpg')" alt="Residential Roofing"> <span>Kitchen Remodeling</span> </a>
                        </li>
                        <li data-item="i">
                            <a href="/bathroom-services/"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/ctas/general-remodeling.jpg')" alt="Residential Roofing"> <span>Bathroom Remodel</span> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="content-area t3 light-bg v1-btn bg-image flex-reverse" id="ContentAreaT3" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/panels/Panel-2-bg.jpg');" data-onvisible="slide-n-fade">
            <div class="main">
                <div class="flex spaced">
                    <div class="content half dark-bg" data-content="true">
                        <header id="ContentAreaT3Header">
                            <h1> <strong>WE ARE <br>PRO ESTIMATOR</strong></h1>
                        </header>
                        <div class="content-style" id="ContentAreaT3Content">
                            <h2>Over the 10 years Satisfied Customers Serviced Throughout USA</h2>
                            <p>If that's not enough, all you need to do is read through the testimonials left by our happy clients. We are proud of the loyal client base that we have built over the years, and we look forward to helping you in the same way.</p>
                        </div>
                        <div id="OptionalContentAreaT3Btn">
</div>
                    </div>
                    <ul class="values-list half" id="ContentAreaT3ValuesList">
                        <li data-item="i">
                            <icon svg="62114">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_62114"></use>
                                </svg>
                            </icon>
                            <strong>We've Serviced <br>over 85,000 <br>Happy Clients</strong>
                        </li>
                        <li data-item="i">
                            <icon svg="60722">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_60722"></use>
                                </svg>
                            </icon>
                            <strong>We're Experts in Home Design, Installation & Remodel</strong>
                        </li>
                        <li data-item="i">
                            <icon svg="63607">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_63607"></use>
                                </svg>
                            </icon>
                            <strong>Staff & Technicians <br>With 30+ Years of Experience</strong>
                        </li>
                        <li data-item="i">
                            <icon svg="64576">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_64576"></use>
                                </svg>
                            </icon>
                            <strong>All New Installations Are Warrantied from Best Contractors</strong>
                        </li>
                        <li data-item="i">
                            <icon svg="59687">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_59687"></use>
                                </svg>
                            </icon>
                            <strong>On Acceptable Credit, <br>We Can Arrange Financing</strong>
                        </li>
                        <li data-item="i">
                            <icon svg="64574">
                                <svg viewbox="0 0 1024 1024">
                                    <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_64574"></use>
                                </svg>
                            </icon>
                            <strong>Repair & Maintenance Work Is Warrantied As Well</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <?php echo do_shortcode('[WP-Coder id="3"]'); ?>

        <?php echo do_shortcode('[WP-Coder id="1"]'); ?>

        <?php echo do_shortcode('[WP-Coder id="2"]'); ?>

        <section class="content-area t5 light-bg v1-btn flex-reverse" id="ContentAreaT5" data-onvisible="slide-n-fade">
            <div class="main">
                <div class="flex spaced">
                    <div class="img half" id="ContentAreaT5Image">
                        <img src="images/spacer.gif" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/pros.jpg')" alt="">
                    </div>
                    <div class="content half" data-content="true">
                        <header id="ContentAreaT5Header">
                            <h4>Contact Our</h4>
                            <h3>Commercial &amp; Residential Pros</h3>
                        </header>
                        <div class="content-style" id="ContentAreaT5Content">
                            <p>We believe that finding the right general contractor and the right quote for all your home Improvement needs should be fast, easy and affordable. That is why we developed Pro Remodeling Quotes, the most efficient way to receive
                                instant online remodeling quotes.</p>
                            <p style="text-align:center;"><strong>Get us for a free estimate for your home needs.</strong></p>
                        </div>
                        <div id="OptionalContentAreaT5Btn">
</div>
                    </div>
                </div>
            </div>
        </section>
        <script id="Process_ContentAreaT5" type="text/javascript" style="display:none;">
            window.Process && Process.Page(['Process_ContentAreaT5', 'ContentAreaT5_1']);
        </script>
        
        <?php echo do_shortcode('[WP-Coder id="4"]'); ?>
        
        <?php echo do_shortcode('[WP-Coder id="5"]'); ?>

        <?php echo do_shortcode('[WP-Coder id="6"]'); ?>
    </main>

    <?php get_footer(); ?>