<?php 

/* Template Name: Inner Page 4 - Search Default */

get_header(); ?>


<main> 


<section class="dark-bg v1-btn bg-image flex-reverse text-left t1 fixed-contact" id="FixedContactStructureForm">
            <?php echo do_shortcode('[contact-form-7 id="297" title="Free Estimate"]'); ?>
        </section>
        <div class="icobalt ilayout" id="MainZone">
            <section class="sub-banner t5 dark-bg bg-image text-left" id="SubBanner" style="background-image:url('http://proremodelingquotes.us/wp-content/uploads/2020/09/sub-banner-bg-dark.jpg')">
                <div class="main">
                    <div class="info title-font">
                        <strong class="header-flair">Search Results</strong>
                    </div>
                </div>
            </section>
        </div>         
        <section class="wide-content-area content-style light-bg" id="WideContentArea">
            <article class="main thin" id="MainContent" data-content="true">
                
            <?php
$s=get_search_query();
$args = array(
                's' =>$s
            );
    // The Query
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
        _e("<h2 style='font-weight:bold;color:#000'>Search Results for: ".get_query_var('s')."</h2>");
        while ( $the_query->have_posts() ) {
           $the_query->the_post();
                 ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                 <?php
        }
    }else{
?>
        <h2 style='font-weight:bold;color:#000'>Nothing Found</h2>
        <div class="alert alert-info">
          <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
        </div>
<?php } ?>



            </article>
        </section>    

        <?php echo do_shortcode('[WP-Coder id="8"]'); ?>



    </main> 

<?php get_footer(); ?>