<footer class="icobalt ilayout" id="FooterZone">
        <div class="footer t5 dark-bg flex-reverse" id="Footer">
            <div class="main flex">
                <div class="footer-info flex three-fifths spaced">
                    <div class="schema-info half" itemscope="" itemtype="#">
                        <div class="business-info">
                            <img itemprop="image" class="bottom-logo" alt="Pro Remodeling Quotes" title="Pro Remodeling Quotes" src="<?php echo get_template_directory_uri(); ?>/images/ProRemodelingLogo2.png">
                            <meta itemprop="name" content="Pro Remodeling Quotes, TTG LLC"/>
                            <meta itemprop="description" content="Free Home Remodeling Quotes"/>
                            <meta itemprop="url" content="#"/>
                        </div>
                        <div class="contact-info">
                            <a class="phone-link" href="tel:818.732.8253" id="Footer_1" data-replace-href="tel:{F:P2:Cookie:PPCP1/818.732.8253}"><span itemprop="telephone"><span id="Footer_2" data-process="replace" data-replace="{F:P:Cookie:PPCP1/(818)%20732-8253}">(818) 732-8253</span></span></a>
                            <a class="phone-link" href="tel:" id="Footer_3" data-replace-href="tel:{F:P2:Cookie:PPCP2/}"><span itemprop="telephone"><span id="Footer_4" data-process="replace" data-replace="{F:P:Cookie:PPCP2/}"></span></span></a>
                        </div>
                        <div class="location-info">
                            <span itemprop="address" itemscope="" itemtype="#"><span itemprop="streetAddress">2315 N San Fernando Blvd  </span> <br> <span itemprop="addressLocality">Burbank</span>, <span itemprop="addressRegion">CA </span> <span itemprop="postalCode">91504</span></span>
                            <br>
                        </div>
                        <div class="social-info">
                            <ul class="items-4" maxresults="8" id="FooterT5Social">
                                <li class="item-1" data-item="i" data-key="44754">
                                    <a itemprop="sameAs" href="https://www.facebook.com/proremodelingquotes/" title="Facebook" target="_blank" rel="nofollow"> <icon class="facebook" title="Facebook"></icon> </a>
                                </li>
                                <li class="item-2" data-item="i" data-key="44755">
                                    <a itemprop="sameAs" href="https://www.instagram.com/proremodelingquotes/" title="Instagram" target="_blank" rel="nofollow"> <icon class="instagram" title="Twitter"></icon> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <nav class="footer-nav half" id="FooterT5Links">
                        <span class="title-font"><strong>Quick Links</strong></span>
                        <ul>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/">Home</a>
                            </li>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/all-services/">All Services</a>
                            </li>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/residential-services/">Residential Services</a>
                            </li>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/request-an-estimate/">Request an Estimate</a>
                            </li>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/contact-us/">Contact Us</a>
                            </li>
                            <li data-item="i">
                                <a href="//proremodelingquotes.us/site-map/">Site Map</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="footer-callout title-font two-fifths" id="FooterT5Callout">
                    <p>Providing Best Costs Services Since<br><strong>10+ Years</strong> </p>
                </div>
            </div>
            <div class="ie-popup modal" id="IEPopup">
                <div class="bg"></div>
                <div class="container">
                    <div class="modal-btn">
                        x
</div>
                    <div class="content">
                        <p class="icon">⚠</p>
                        <p>Your browser is out of date. To get the full experience of this website, please update to most recent version.</p>
                    </div>
                    <a href="#" target="_blank" rel="nofollow" class="btn v1 light">Update My Browser</a>
                </div>
            </div>
        </div>
        <script id="Process_Footer" type="text/javascript" style="display:none;">
            window.Process && Process.Page(['Process_Footer', 'Footer_1', 'Footer_2', 'Footer_3', 'Footer_4', 'Footer_5']);
        </script>
        <section class="scorpion-footer t1 light-bg" id="ScorpionFooter">
            <div class="main">
                <div class="flex">
                    <div class="copyright">
                        Pro Remodeling Quotes
                        <div>
                            &copy; 2019 All Rights Reserved.
</div>
                    </div>
                </div>
            </div>
        </section>
    </footer>
    <script type="text/javascript">
        rrequire('form', function() {
            $('#Form_FixedContactStructureForm').html5form();
        });
    </script>
    <script type="text/javascript">
        rrequire('form', function() {
            $('#Form_SearchBannerT1').html5form();
        });
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/form.js" defer data-require='["sa"]'></script>
    <noscript id="deferred-styles">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700|Poppins:300,400,600,700" data-font="google"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/poppins-nunito-sans.css" data-font="css"/>
    </noscript>
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById('deferred-styles');
            var replacement = document.createElement('div');
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
        if (raf) raf(function() {
            window.setTimeout(loadDeferredStyles, 0);
        });
        else window.addEventListener('load', loadDeferredStyles);
    </script>
</body>