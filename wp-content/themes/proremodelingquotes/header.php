<!DOCTYPE html> 
<meta http-equiv="content-type" content="text/html;charset=utf-8"/> 
<head>
    <title>Pro Remodeling Quotes &mdash; Free Quotes from trusted Pros for window replacement cost, roofing cost, flooring cost, solar panels cost and more!</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="initial-scale=1" name="viewport"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <script type="text/javascript">
        (function(n){var t=n.Process||{},i=function(n){var t=+n;return isNaN(t)?n:t},u=function(n){return decodeURIComponent(n.replace(/\+/g,"%20"))},h=/\{(.+?)\}/g,c=window.location.hash&&window.location.hash[1]==="~"&&!/\bSPPC=./i.test(document.cookie||""),s=[],e=null,r=null,o=null,f=null;t.Page=function(n){for(var i=n.length;i--;)c?s.push(n[i]):t.Element(document.getElementById(n[i]))};t.Delayed=function(){var n;for(r=null;n=s.shift();)t.Element(document.getElementById(n))};t.Element=function(n){if(n)switch(n.getAttribute("data-process")){case"if":t.If(n);break;case"replace":t.Replace(n);break;default:t.Fix(n)}};t.Replace=function(n){var i,f=n.parentNode,r=document.createTextNode(t.Get(n.getAttribute("data-replace"))),u=n.firstElementChild;u&&u.getAttribute&&(i=u.getAttribute("href"))&&i.substring(0,4)==="tel:"&&(i=document.createElement("a"),i.setAttribute("href","tel:"+r.data),i.appendChild(document.createTextNode(r.data)),r=i);f.insertBefore(r,n);f.removeChild(n)};t.Fix=function(n){var r,u,i,f=n.attributes.length,e=n.childNodes.length;if(n.nodeName==="SCRIPT"){n.parentNode.removeChild(n);return}while(f--)r=n.attributes[f],r.name.substring(0,13)=="data-replace-"&&(u=r.name.substring(13),n.setAttribute(u,t.Get(r.value)),n.removeAttribute(r.name));while(e--)i=n.childNodes[e],i.nodeType===3&&i.data&&i.data.indexOf("{")>=0&&(i.data=t.Get(i.data))};t.If=function(n){for(var i,u,f,e,o=n.parentNode,s=n.attributes.length,r=undefined;s--;){i=n.attributes[s];switch(i.name){case"field":r=t.Check(n,t.Get(i.value));break;case"nofield":r=!t.Check(n,t.Get(i.value))}if(r!==undefined)break}if(r)for(u=n.childNodes,f=0,e=u.length;f<e;f++)o.insertBefore(u[0],n);o.removeChild(n)};t.Check=function(n,r){for(var u,f,e=n.attributes.length;e--;){u=n.attributes[e];switch(u.name){case"equals":return r==t.Get(u.value);case"gt":case"greaterthan":case"morethan":return i(r)>i(t.Get(u.value));case"gte":return i(r)>=i(t.Get(u.value));case"lt":case"lessthan":case"lesserthan":return i(r)<i(t.Get(u.value));case"lte":return i(r)<=i(t.Get(u.value));case"ne":case"notequals":return r!=t.Get(u.value);case"contains":return f=t.Get(u.value),r.indexOf(f>=0);case"notcontains":return f=t.Get(u.value),!r.indexOf(f>=0);case"in":return f=t.Get(u.value),t.InArray(r,(""+f).split(","));case"notin":return f=t.Get(u.value),!t.InArray(r,(""+f).split(","));case"between":return f=t.Get(u.value).Split(","),f.length==2&&i(r)>=i(f[0])&&i(r)<=i(f[1])?!0:!1}}return!!r};t.InArray=function(n,t){for(var i=t.length;i--;)if(t[i]==n)return!0;return!1};t.Get=function(n){return n.replace(h,function(n,i){var r=i.split("index.html"),f=r.shift();return t.Data(f.split(":"),0,r[0])||u(r.shift()||"")})};t.Data=function(n,i,r){var u;switch(n[i].toLowerCase()){case"f":return t.Format(n[i+1],n,i+2,r);case"if":return t.Data(n,i+1)?n.pop():"";case"ifno":case"ifnot":return t.Data(n,i+1)?"":n.pop();case"q":case"querystring":return t.Query(n[i+1])||"";case"session":case"cookie":return t.Cookie(n[i+1])||"";case"number":return t.Number(n[i+1],r)||"";case"request":return u=t.Cookie("RWQ")||window.location.search,u&&u[0]==="?"&&n[i+1]&&n[i+1][0]!="?"&&(u=u.substr(1)),u;default:return""}};t.Format=function(n,i,r,u){var h,f,s,e,o;if(!n||r>i.length-1)return"";if(h=null,f=null,n=n.toLowerCase(),e=0,n=="binary")e=2;else if(r+1<i.length)switch(n){case"p":case"phone":case"p2":case"phone2":case"p3":case"phone3":i[r].indexOf("0")>=0&&(f=i[r],e=1);break;default:s=parseInt(i[r]);isNaN(s)||(h=s,e=1)}o=t.Data(i,r+e,u);switch(n){case"p":case"phone":return t.Phone(""+o,f);case"p2":case"phone2":return t.Phone(""+o,f||"000.000.0000");case"p3":case"phone3":return t.Phone(""+o,f||"000-000-0000");case"tel":return t.Phone(""+o,f||"0000000000")}};t.Phone=function(n,t){var u,i,f,r;if(!n)return"";if(u=n.replace(/\D+/g,""),u.length<10)return n;for(i=(t||"(000) 000-0000").split(""),f=0,r=0;r<i.length;r++)i[r]=="0"&&(f<u.length?i[r]=u[f++]:(i.splice(r,1),r--));return f==10&&u.length>10&&i.push(" x"+u.substring(10)),i.join("")};t.Query=function(n){var r,f,o,i,s;if(!e)for(e={},r=t.Cookie("RWQ")||window.location.search,f=r?r.substring(1).split("&"):[],o=f.length;o--;)i=f[o].split("="),s=u(i.shift()).toLowerCase(),e[s]=i.length?u(i.join("=")):null;return e[n.toLowerCase()]};t.Cookie=function(n){var f,i,o,e,t;if(!r)for(r={},f=document.cookie?document.cookie.split("; "):[],i=f.length;i--;){o=f[i].split("=");e=u(o.shift()).toLowerCase();t=o.join("=");switch(t[0]){case"#":r[e]=+t.substring(1);break;case":":r[e]=new Date(+t.substring(1));break;case"!":r[e]=t==="!!";break;case"'":r[e]=u(t.substring(1));break;default:r[e]=u(t)}}for(f=n.split("|"),i=0;i<f.length;i++)if(t=r[f[i].toLowerCase()],t)return t;return""};t.Number=function(n,i){var s,u,r,e;if(!i)return i;if(!o)for(o={},s=(t.Cookie("PHMAP")||"").split(","),e=0;e<s.length;e++)u=(s[e]||"").split("="),u.length===2&&(o[u[0]]=u[1]);return r=o[i],r&&r!=="0"||(r=i),f||(f={}),f[r]=1,r};t.Phones=function(){var n,t;if(f){n=[];for(t in f)f.hasOwnProperty(t)&&n.push(t);return n.join("|")}return null};n.Process=t;document.documentElement&&(document.documentElement.clientWidth<=1280||(t.Cookie("pref")&1)==1)&&(document.documentElement.className+=" minimize")})(this);
        (function(n){window.rrequire||n(window)})(function(n){function w(i,r,u,f){var o,w,e,a,d,h,g=(new Date).getTime(),y,c,l,p;if(((!s||g-s>1e3)&&(ft(),s=g),!t[i])&&(o=b(i,f),o.length)){switch(o[0]){case"/common/js/j/jquery.js":case"/common/js/j/jquery.1.x.js":case"/common/js/j/jquery.2.x.js":o[0]=n.Modernizr&&n.Modernizr.canvas?"/common/js/j/jquery.2.x.js":"/common/js/j/jquery.1.x.js"}if(k(o),f==="css"?(e=n.document.createElement("link"),e.setAttribute("type","text/css"),e.setAttribute("rel","stylesheet"),a="href"):(e=n.document.createElement("script"),e.setAttribute("type","text/javascript"),e.setAttribute("async","async"),a="src",d=o.length<2||i[0]==="index.html"),i.indexOf("j/jquery")>=0)try{throw new Error("Jquery Require "+i);}catch(nt){for(console.log(nt.stack),y=[],c=arguments;c;)l=[],l.push.apply(l,c),y.push(l),p=c.callee.caller,c=p&&p.arguments;console.log(JSON.stringify(y))}h=o[0];h.substring(0,8)==="/common/"&&(h=v+h);ut(e,i,f,d);e.setAttribute(a,h);w=n.document.head||n.document.body;w.appendChild(e)}}function ut(t,i,r,e){var s=function(){e?(n.register(i),l()):r==="css"&&(f[i]=!0,l())},o=function(){r==="js"&&console.log('error - "'+i+'" could not be loaded, rrequire will not fire.')};t.addEventListener?(t.addEventListener("load",s,!1),t.addEventListener("error",o,!1)):t.onload=t.onreadystatechange=function(n,r){var f=t.readyState;if(r||!f||/loaded|complete/.test(f)){if(f==="loaded"&&(t.children,t.readyState==="loading"&&(f="error")),t.onload=t.onreadystatechange=null,t=null,e&&f!=="error"){setTimeout(function(){var n=u[i];!n||n()?s():o();s=null;o=null},1);return}f==="error"?o():s();o=null;sucess=null}}}function ft(){for(var t,r,u,i=document.querySelectorAll("script[src]"),n=0;n<i.length;n++)t=i[n],r=t.getAttribute("src"),u=t.getAttribute("data-require"),c(r,u,"js");for(i=document.querySelectorAll("link[rel='stylesheet'][href]"),n=0;n<i.length;n++)t=i[n],r=t.getAttribute("href"),u=t.getAttribute("data-require"),c(r,u,"css")}function b(t,i){var r=[];if(!t)return r;if(t.indexOf(n.location.origin)===0&&(t=t.substring(n.location.origin.length)),m=rt.exec(t)){if(m[1])return r.push(t),r;if(m[2])return r.push(n.location.protocol+t),r;(m=/(.+?)\.\d{13}(\.\w{2,12})$/.exec(t))&&(t=m[1]+m[2]);r.push(t);(m=/^\/(common|cms)\/(admin\/|js\/|css\/)?(.+?)(\.js|\.css)$/.exec(t))&&(t=m[1]==="cms"?m[1]+"/"+(m[2]||"")+m[3]:m[2]==="admin/"?m[2]+m[3]:m[3],r.push(t))}else/^cms\//.test(t)?r.push("/"+t+"."+i):/^admin\//.test(t)?r.push("/common/"+t+"."+i):i==="js"?r.push("/common/js/"+t+"."+i):i==="css"&&r.push("/common/css/"+t+"."+i),r.push(t);return r}function c(n,i,r){var f,u;if(n&&!t[n]&&(f=b(n,r),k(f),i)){try{arr=JSON.parse(i)}catch(e){return}for(len=arr&&arr.length,u=0;u<len;u++)c(arr[u],null,r)}}function k(n,i){for(var r,u=0;u<n.length;u++){r=n[u];switch(r){case"j/jquery":case"j/jquery.1.x":case"j/jquery.2.x":case"j/jquery.3.x":t["j/jquery"]=!0;t["j/jquery.1.x"]=!0;t["j/jquery.2.x"]=!0;t["j/jquery.3.x"]=!0;t["/common/js/j/jquery.js"]=!0;t["/common/js/j/jquery.1.x.js"]=!0;t["/common/js/j/jquery.2.x.js"]=!0;t["/common/js/j/jquery.3.x.js"]=!0;break;case"cms":case"cms-5":i==="css"?(t.cms=!0,t["cms-5"]=!0,t["/common/css/cms.css"]=!0,t["/common/css/cms-5.css"]=!0):t[r]=!0;break;default:t[r]=!0}}}function d(n){for(var t,i=n.length;i--;)if(t=n[i],t&&!f[t])return!1;return!0}function et(){var t,n=r.length;for(e&&(clearTimeout(e),e=0);n--;)t=r[n],t[2]===!0&&r.splice(n,1);r.length===0&&document.documentElement.classList&&document.documentElement.classList.remove("requiring")}function l(){for(var t,i=0,u=r.length;i<u;)(t=r[i++],t[2]!==!0)&&d(t[0])&&(t[2]=!0,t[1](n.jQuery,n),e&&clearTimeout(e),e=setTimeout(et,1))}function g(n){var t;if(n){if(typeof n=="string")return[n.toLowerCase()];if(Array.isArray(n)){for(t=n.length;t--;)n[t]=(""+(n[t]||"")).toLowerCase();return n}return null}return null}function nt(n,t){for(var i,r,u=0;u<n.length;u++)if(i=h[n[u]],i){for(n.splice(u,1),u--,r=i[0].length;r--;)n.push(i[0][r]);if(i[1]&&t)for(r=i[1].length;r--;)t.push(i[1][r])}t&&t.length&&nt(t)}var i="index.html",o=".",a=":",v=i+i+"www.scorpioncms.com",ot=i+"cms"+i,f={},t={},r=[],e=0,y=document&&document.documentElement&&document.documentElement.getAttribute("data-gmap"),tt=y&&"&key="+y,p=Object.prototype,st=p.toString,it=p.hasOwnProperty,h={jquery:[["j/jquery","j/jquery.ui"]],behavior:[["behaviors"],["cms-behave"]],googlemap:[["https"+a+i+i+"maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=registermap"+(tt||"")]],map:[["m/gmap"]],loading:[["c/loading2"],["cms-5"]],jwplayer:[[i+"common/js/v/jwplayer"+o+"js"]],tools:[["jquery","behavior","extensions","uri","chart","c/cms","c/scrollbar","loading","form"],["cms-tools","opensans"]],opensans:[["https"+a+i+i+"fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,700|Montserrat:400,700"]],ckeditor:[[o+o+i+"ckeditor/ckeditor"]],ck:[["admin/ck/ckeditor"]],ace:[[i+i+"cdnjs.cloudflare.com/ajax/libs/ace/1.4.1/ace"+o+"js"]],weather:[["m/weather"]],cookie:[["j/jquery.cookie"]],form2:[["admin/js/form","admin/js/poly"]]},u={"j/jquery":function(){return!!n.jQuery},"j/jquery.1.x":function(){return!!n.jQuery},"j/jquery.2.x":function(){return!!n.jQuery},"j/jquery.3.x":function(){return!!n.jQuery},"j/jquery.ui":function(){return!!(n.jQuery&&n.jQuery.widget)},"j/jquery.cookie":function(){return!!(n.jQuery&&n.jQuery.cookie)},"j/poly":function(){return!!(n.Element&&n.Element.prototype&&n.Element.prototype.scrollIntoViewport)},googlemap:function(){return!!(n.google&&n.google.maps)},jwplayer:function(){return!!n.jwplayer},ckeditor:function(){return!!n.CKEDITOR},ace:function(){return!!n.ace},weather:function(){return!!(n.jQuery&&n.jQuery.weather)}},rt=/^(https?:)?(\/\/([\w\-\.]+))?(\/.+)/i,s;(function(){var n;for(var t in u)it.call(u,t)&&(n=h[t],n&&n[0]&&n[0][0]&&(u[n[0][0]]=u[t]))})();Array.isArray||(Array.isArray=function(n){return Object.prototype.toString.call(n)==="[object Array]"});Function.isFunction||(Function.isFunction=function(n){return Object.prototype.toString.call(n)==="[object Function]"});s=null;n.registerLoading=function(n){t[n]=!0};n.register=function(n){n&&typeof n=="string"&&(t[n]=!0,f[n]=!0,l())};n.registermap=function(){var n=h.googlemap[0][0];register(n)};n.rrequire=function(i,e,o){var a,s,v,c,h,y,l;if(i=g(i),i){for(Function.isFunction(o)&&(a=o,o=e,e=a,a=null),o=g(o),o||(o=[]),nt(i,o),s=i.length,v=!0;s--;)if((c=i[s],c)&&(h=c.toLowerCase(),!f[h])){if(y=u[h],y&&y()){f[h]=!0;continue}v=!1;t[h]||w(c,"script","src","js")}for(s=0;s<o.length;)(l=o[s],l)&&(h=l.toLowerCase(),f[h]||w(l,"link","href","css"),s++);Function.isFunction(e)&&(v||d(i)?e(n.jQuery,n):r.push([i,e,!1]))}};n.rrequire.setBase=function(n){v=n};n.rrequire.setDetect=function(n,t){n&&typeof n=="string"&&Function.isFunction(t)&&(u[n]=t)};n.rrequire.getLoading=function(){var n=Object.keys(t);return n.sort(),console.log(JSON.stringify(n,null,"\t"))};n.require||(n.require=n.rrequire)});
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" data-require='["cms","cms-behave"]'/>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js" defer data-require='["j/poly","j/modernizr","j/jquery","j/jquery.ui","j/ui.touch","j/ui.wheel","j/ui.draw","j/ui.mobile","j/timezone","static","j/jquery.cookie","extensions","uri","behaviors","c/scrollbar","c/loading","m/date","form","adapter","v/jwplayer","video","a/bootstrap","svg"]'></script>

    <?php wp_head(); ?>
</head> 
<body class="<?php the_field( 'class' ); ?>" data-config="lub34ny4.lnx"> 
    <header class="icobalt ilayout" id="HeaderZone">
        <div class="header-area t6 light-bg" id="HeaderArea">
            <a class="top-logo" href="/"><img alt="Pro Remodeling Quotes" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" title="Pro Remodeling Quotes"></a>
            <div class="header-box">
                <div class="top-bar flex" id="HeaderAreaT6TopBarContent">
                    <nav class="secondary-nav">
                        <ul>
                            <li data-item="i">
                                <a href="/">Home</a>
                            </li>
                            <li data-item="i">
                                <a href="/request-an-estimate/">Request an Estimate</a>
                            </li>
                        </ul>
                    </nav>
                    <strong>America's Best Remodeling Quotes Company</strong>
                </div>
                <div class="nav-bar flex">
                    <nav class="top-nav">
                        <ul class="desktop-nav container" id="HeaderAreaT6TopNavigation">
                            <li class="about-delta  child1">
                                <a href="/about/" target=""><span>About</span></a>
                            </li>                             
                            <li class="commercial  child5">
                                <a target="" href="/windows-services/"><span>Windows</span></a>
                                <icon svg="59665" class="panel-btn">
                                    <svg viewbox="0 0 1024 1024">
                                        <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_59665"></use>
                                    </svg>
                                </icon>
                                <ul class="fly-nav" id="Ddcommercial" data-role="fly-nav">
                                    <?php
                                            $args = array(
                                            'post_type' => 'windows',
                                            );
                                            $cpt_list = new WP_Query( $args );
                                            if( $cpt_list->have_posts() ) {
                                                while( $cpt_list->have_posts() ) {
                                                    $cpt_list->the_post();
                                                    ?>
                                                    
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                                    <?php
                                                }
                                            }
                                    ?>
                                    <li>
                                        <a href="/windows-services/">Other Types of Windows</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="residential  child5">
                                <a target="" href="/roofing-services/"><span>Roofs</span></a>
                                <icon svg="59665" class="panel-btn">
                                    <svg viewbox="0 0 1024 1024">
                                        <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_59665"></use>
                                    </svg>
                                </icon>
                                <ul class="fly-nav" id="Ddresidential" data-role="fly-nav">
                                    <?php
                                            $args = array(
                                            'post_type' => 'roofs',
                                            );
                                            $cpt_list = new WP_Query( $args );
                                            if( $cpt_list->have_posts() ) {
                                                while( $cpt_list->have_posts() ) {
                                                    $cpt_list->the_post();
                                                    ?>
                                                    
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                                    <?php
                                                }
                                            }
                                    ?>
                                    <li class="">
                                        <a href="/roofing-services/">Other Types of Roofs</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="residential  child5">
                                <a target="" href="/flooring-services/"><span>Floors</span></a>
                                <icon svg="59665" class="panel-btn">
                                    <svg viewbox="0 0 1024 1024">
                                        <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_59665"></use>
                                    </svg>
                                </icon>
                                <ul class="fly-nav" id="Ddresidential" data-role="fly-nav">
                                    <?php
                                            $args = array(
                                            'post_type' => 'floors',
                                            );
                                            $cpt_list = new WP_Query( $args );
                                            if( $cpt_list->have_posts() ) {
                                                while( $cpt_list->have_posts() ) {
                                                    $cpt_list->the_post();
                                                    ?>
                                                    
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                                    <?php
                                                }
                                            }
                                    ?>
                                    <li>
                                        <a href="/flooring-services/">Other Types of Floors</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="maintenance  child0">
                                <a target="" href="/solar-services/"><span>Solars</span></a>
                            </li>
                            <li class="inspections  child0">
                                <a target="" href="/hvac-systems/"><span>HVAC</span></a>
                            </li>
                            <li class="inspections  child0">
                                <a href="/kitchen-services/" target=""><span>Kitchen</span></a>
                            </li>
                            <li class="inspections  child0">
                                <a href="/bathroom-services/" target=""><span>Bathroom</span></a>
                            </li>
                        </ul>
                    </nav>
                    <ul class="mobile-nav">
                        <li class="mobile-call">
                            <a href="tel:(818) 732-8253" title="Call Now" id="HeaderArea_3" data-replace-href="tel:{F:P:Cookie:PPCP1/(818)%20732-8253}"><icon svg="64193">
                                    <svg viewbox="0 0 1024 1024">
                                        <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_64193"></use>
                                    </svg>
                                </icon> Call </a>
                        </li>
                        <li>
                            <a class="menu-btn" role="button" href="javascript:void(0);"><icon svg="64059">
                                    <svg viewbox="0 0 1024 1024">
                                        <use data-href="<?php echo get_template_directory_uri(); ?>/images/larylrha3gl.svg#icon_64059"></use>
                                    </svg>
                                </icon> Menu</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <script id="Process_HeaderArea" type="text/javascript" style="display:none;">window.Process&&Process.Page(['Process_HeaderArea','HeaderArea_1','HeaderArea_2','HeaderArea_3']);</script>
    </header>    