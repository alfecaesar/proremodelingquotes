<?php 

/* Template Name: Inner Page 2 - Overview and Services List */

get_header(); ?>
<main class="icobalt ilayout" id="MainZone">
<?php 
                if (have_posts()) :
                    while (have_posts()) :
                        the_post();
        ?>
<section class="dark-bg v1-btn bg-image flex-reverse text-left t1 fixed-contact" id="FixedContactStructureForm">
            <?php echo do_shortcode('[contact-form-7 id="297" title="Free Estimate"]'); ?>
        </section>
        <script id="Process_ContentAreaT5" type="text/javascript" style="display:none;">window.Process&&Process.Page(['Process_ContentAreaT5','ContentAreaT5_1']);</script>

        <section class="services-area t5 bg-image v1-btn text-center dark-bg" id="ServicesArea" style="background-image:url('<?php $banner_image = get_field( 'banner_image' ); if ( $banner_image ){echo esc_url( $banner_image['url'] ); } ?>');" data-onvisible="slide-n-fade">
            <div class="main">
                
                       
                          <?php the_content(); ?>
                    
            </div>
    </section>


    <?php echo do_shortcode('[WP-Coder id="6"]'); ?>

    <?php 
                    endwhile;
                endif;
    ?>
</main>   

<?php get_footer(); ?>