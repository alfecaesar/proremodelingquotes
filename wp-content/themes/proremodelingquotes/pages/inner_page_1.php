<?php 

/* Template Name: Inner Page 1 - About Us */

get_header(); ?>


<main> 
<?php 
                if (have_posts()) :
                    while (have_posts()) :
                        the_post();
        ?>
        <div class="icobalt ilayout" id="MainZone">
            <section class="sub-banner t5 dark-bg bg-image text-left" id="SubBanner" style="background-image:url('<?php $banner_image = get_field( 'banner_image' ); if ( $banner_image ){echo esc_url( $banner_image['url'] ); } ?>')">
                <div class="main">
                    <div class="info title-font">
                        <strong class="header-flair"><?php the_field( 'banner_title' ); ?></strong>
                    </div>
                </div>
            </section>
        </div>         
        <section class="main two-columns-container flex reverse spaced"> 
            <article class="ilayout icobalt" id="ContentZone">
                <div class="column-layout-content content-style light-bg" id="ColumnLayoutContent">
                    <div id="MainContent" data-content="true">
                        <?php the_content(); ?>
                        
                    </div>
                </div>
                <script id="Process_ColumnLayoutContent" type="text/javascript" style="display:none;">window.Process&&Process.Page(['Process_ColumnLayoutContent','ColumnLayoutContent_1']);</script>
            </article>             
        </section>      

        <?php echo do_shortcode('[WP-Coder id="8"]'); ?>

        <?php 
                    endwhile;
                endif;
    ?>

    </main> 

<?php get_footer(); ?>